﻿using System.Data;
using System.Reflection.Emit;
using Unigine;
using Console = System.Console;

public abstract class BaseGui
{
    private string PageName { get; } = "BaseGui";

    public abstract string GetPageName();

    // Relatives to UI folder inside data/UI
    private string PageLink { get; } = "None";

    public abstract string GetPageLink();

    private UserInterface userInterface = null;

    protected UserInterface GetUserInterface()
    {
        return userInterface;
    }

    public BaseGui()
    {
        BuildUi();
        GuiController.AddPage(this);
    }

    public virtual void Update()
    {
        
    }

    ~BaseGui()
    {
    }

    private void BuildUi()
    {
        Console.WriteLine("Create widget");
        if (GetPageLink().Equals("None"))
        {
            Log.Error($"BaseGui.BuildUI(): PageLink.Equals(\"GetPageLink() = {GetPageLink()}\"). " +
                      $"Build was failed.\n");
            return;
        }

        var gui = Gui.Get();
        userInterface = new UserInterface(gui, $"MagicFirst/UI/{GetPageLink()}");
        if (!userInterface.IsValidPtr)
        {
            Log.Error($"BaseGui.BuildUI(): ui have is not valid ptr, returning...\n");
            return;
        }
        var window2 = userInterface.GetWidget(userInterface.FindWidget("Window"));
        userInterface.GetWidget(0);
        window2.Arrange();
        gui.AddChild(window2);
    }
}