/* Copyright (C) 2005-2020, UNIGINE. All rights reserved.
 *
 * This file is a part of the UNIGINE 2.11.0.1 SDK.
 *
 * Your use and / or redistribution of this software in source and / or
 * binary form, with or without modification, is subject to: (i) your
 * ongoing acceptance of and compliance with the terms and conditions of
 * the UNIGINE License Agreement; and (ii) your inclusion of this notice
 * in any version of this software that you use or redistribute.
 * A copy of the UNIGINE License Agreement is available by contacting
 * UNIGINE. at http://unigine.com/
 */


using System;
using Unigine;

class UnigineApp
{
	private static void edittext_changed(Widget widget)
	{
		WidgetEditText edittext = widget as WidgetEditText;
		Log.Message("EditText changed: {0}\n", edittext.Text);
	}

	private static void menubox_0_clicked(Widget widget)
	{
		WidgetMenuBox menubox = widget as WidgetMenuBox;
		Log.Message("MenuBox clicked: {0}\n", menubox.CurrentItemText);
		if (menubox.CurrentItem == 2)
		    Unigine.Console.Run("quit");
	}

	class AppSystemLogic : SystemLogic
	{
		private UserInterface ui;

		public override bool Init()
		{
			Gui gui = Gui.Get();

			ui = new UserInterface(gui, "data/user_interface.ui");

			ui.AddCallback("edittext", Gui.CALLBACK_INDEX.CHANGED, edittext_changed);
			ui.AddCallback("menubox_0", Gui.CALLBACK_INDEX.CLICKED, menubox_0_clicked);

			Widget window = ui.GetWidget(ui.FindWidget("window"));
			window.Arrange();
			gui.AddChild(window, Gui.ALIGN_OVERLAP | Gui.ALIGN_CENTER);

			return true;
		}
	}

	[STAThread]
	static void Main(string[] args)
	{
		// init engine
		Engine.Init("UNIGINE Engine: Creating user interface and adding callbacks for MenuBox and EditText (C#)", args);
		
		// enter main loop
		AppSystemLogic system_logic = new AppSystemLogic();
		Engine.Main(system_logic, null, null);

		// shutdown engine
		Engine.Shutdown();
	}
}
