<h3>Установка</h3>

1. Создать проект Unigine с именем MagicFirst, в какой-нибудь отдельной папке, с другим названием.
2. Выполнить клонирование репозитория в пустую папку с именем MagicFirst.
3. Скопировать папку ```bin``` из созданной папки с проектом, в склонированную папку MagicFirst.
4. В Unigine SDK Browser нажать ```Add Existing```, после нажать на кнопку ```Browse``` и выбрать папку MagicFirst.
5. В Unigine SDK Browser удалить старый проект(MagicFirst).
<br><br>(Опционально, для плагинов)
6. Модифицировать unigine.cfg добавлением ```<item name="extern_plugin" type="string">../plugins/uep/bin/uep</item>```
7. В Unigine SDK Browser под кнопкой проект "Open Editor" нажать на кнопку с текстом "..." и добавить в Additional arguments ```-extern_plugin ../plugins/uep/bin/uep```
<br><br>(Опционально, для программистов)
8. Модифицировать проект в Visual Studio/Rider

<h3>Setup</h3>

1. Create project Unigine with name MagicFirst, in any other place.
2. Clone repository in empty folder with name MagicFirst.
3. Copy folder ```bin``` from created project folder, to cloned MagicFirst.
4. In Unigine SDK Browser click at ``Add Existing```, after click at ```Browse``` and select folder MagicFirst.
5. In Unigine SDK Browser remove old project(MagicFirst)
<br><br>(Optionally, for plugins)
6. Modify unigine.cfg by adding ```<item name="extern_plugin" type="string">../plugins/uep/bin/uep</item>```
7. In Unigine SDK Browser under button of project "Open Editor" press at button with name "..." and add in Additional arguments ```-extern_plugin ../plugins/uep/bin/uep```
<br><br>(Optionally, for programmers)
8. Modify project in Visual Studio/Rider